# Daily Report (2023/07/26)

## O

### Code Review

**During today's code review phase, I noticed that React listening mechanism differs from Vue. By default, React employs deep listening, meaning that if a property of an object within an array changes, React will re-render all the DOM elements related to that array. On the other hand, Vue does not automatically detect such changes unless deep listening is explicitly enabled. As a result, without deep listening enabled in Vue, changes to the mentioned property in an array would not trigger reactivity.**

### Axios

##### Learning to use Axios has been a very rewarding experience. It is straightforward and easy to use, with features like interceptors, error handling, and request cancellation, making handling network requests efficient and elegant. Additionally, its cross-platform compatibility makes it a powerful tool for both frontend and backend development. With Axios, I can easily manage concurrent requests, handle file uploads and downloads, and achieve reliable data exchange in my applications.

## R

##### meaningful

## I

##### The most meaningful activity is learning axios.

## D

1. ##### Axios is a Promise-based asynchronous request library. For beginners, understanding and handling asynchronous programming may be challenging.

2. ##### Still confused when writing CSS styles.