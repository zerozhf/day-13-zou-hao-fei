import * as todoService from "../../apis/todo"
import { useDispatch } from "react-redux";
import { initTodoTask } from "../todo/reducers/todoSlice";
export const useTodo = () => {
    const dispatch = useDispatch();
    async function loadTodos() {
        const response = await todoService.getToDoTask();
        dispatch(initTodoTask(response.data))
    }

    async function updateTodo(id, task) {
        await todoService.updateToDoTask(id, task);
        loadTodos();
    }

    async function deleteTodo(id, task) {
        await todoService.deleteToDoTask(id);
        loadTodos();
    }

    async function createTodo(task) {
        await todoService.createToDoTask(task);
        loadTodos();
    }

    async function getTodoById(id) {
        return await todoService.getToDoTaskById(id);
    }



    return {
        loadTodos,
        updateTodo,
        deleteTodo,
        createTodo,
        getTodoById,
    }
}