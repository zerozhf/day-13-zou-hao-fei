import { useTodo } from "../../hooks/useTodo";
import React, { useState } from 'react';
import { Button, Modal, Input } from 'antd';
import { EditOutlined, DeleteOutlined, EyeOutlined } from '@ant-design/icons';

export default function TodoItem(props) {
    const { updateTodo, deleteTodo, getTodoById } = useTodo();
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [taskName, setTaskName] = useState(props.task.name);
    const [isTooltipVisible, setIsTooltipVisible] = useState(false);
    const [todoDetail, setTodoDetail] = useState("")

    const showModal = () => {
        setIsModalOpen(true);
    };

    const handleOk = () => {
        updateTodo(props.task.id, { name: taskName });
        setIsModalOpen(false);
    };

    const handleCancel = () => {
        setTaskName(props.task.name);
        setIsModalOpen(false);
    };

    const handleTaskNameClick = () => {
        updateTodo(props.task.id, { done: !props.task.done });
    };

    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            deleteTodo(props.task.id);
        }
    };

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleTooltipVisible = async () => {
        const response = await getTodoById(props.task.id);
        setTodoDetail(response.data.name);
        setIsTooltipVisible(!isTooltipVisible);
    };

    return (
        <div className='todo-item'>
            <div className={`task-name ${props.task.done ? 'done' : ''}`}
                onClick={handleTaskNameClick}>{props.task.name}
            </div>
            <div>
                <Button className="eye-button" icon={<EyeOutlined />} onClick={handleTooltipVisible} />
                <Button type="primary" icon={<EditOutlined />} onClick={showModal} />
                <Button className="remove-button" type="primary" danger icon={<DeleteOutlined />} onClick={handleRemoveButtonClick} />
                <Modal title="Task Detail" open={isTooltipVisible} onCancel={handleTooltipVisible} footer={null}>
                    <p>{todoDetail}</p>
                </Modal>
                <Modal title="Update TaskName" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
                    <Input type="text" placeholder='input a new todo here...' value={taskName} onChange={handleTaskNameChange} required="required" />
                </Modal>
            </div>
        </div>
    );
}




