import { useState } from "react";
import { useTodo } from "../../hooks/useTodo";
import { Button } from 'antd';

export default function TodoGenerator() {
    const { createTodo } = useTodo()
    const [taskName, setTaskName] = useState("");

    const handleTaskNameChange = (e) => {
        const value = e.target.value;
        setTaskName(value);
    }

    const handleAddTodoTask = async () => {
        createTodo({ done: false, name: taskName })
        setTaskName("");
    }
    return (
        <div className='todo-generator'>
            <input placeholder='input a new todo here...' onChange={handleTaskNameChange} value={taskName}></input>
            <Button onClick={handleAddTodoTask} disabled={!taskName} type="primary" > Add </Button>
        </div>
    );
}