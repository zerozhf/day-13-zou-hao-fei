import { useNavigate } from "react-router-dom";
import { useTodo } from "../../../hooks/useTodo";
import { Button, Modal, } from 'antd';
import React, { useState } from 'react';
import { DeleteOutlined, EyeOutlined } from '@ant-design/icons';

export default function DoneItem(props) {
    const [todoDetail, setTodoDetail] = useState(null)
    const { deleteTodo, getTodoById } = useTodo()
    const [isTooltipVisible, setIsTooltipVisible] = useState(false);
    const navigate = useNavigate();
    const handleTaskNameClick = () => {
        navigate("/done/" + props.task.id);
    }
    const handleRemoveButtonClick = async () => {
        if (window.confirm('Are you sure you wish to delete this item?')) {
            deleteTodo(props.task.id)
        }
    }
    const handleTooltipVisible = async () => {
        const response = await getTodoById(props.task.id);
        setTodoDetail(response.data.name);
        setIsTooltipVisible(!isTooltipVisible);
    };
    return (
        <div className='todo-item'>
            <div className={`task-name ${props.task.done ? 'done' : ''}`} onClick={handleTaskNameClick}
            >{props.task.name}</div>
            <div>
                <Button className="eye-button" icon={<EyeOutlined />} onClick={handleTooltipVisible} />
                <Button className="remove-button" type="primary" danger icon={<DeleteOutlined />} onClick={handleRemoveButtonClick} />
                <Modal title="Task Detail" open={isTooltipVisible} onCancel={handleTooltipVisible} footer={null}>
                    <p>{todoDetail}</p>
                </Modal>

            </div>
        </div>
    );
}