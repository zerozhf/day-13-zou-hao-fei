import DoneItem from './DoneItem';
import { useSelector } from 'react-redux'
export default function DoneGroup() {
    const todoTasks = useSelector(state => state.todo.tasks).filter(task => task.done);
    return (
        <div className='todo-group'>
            {todoTasks.map(((todoTask) =>
                <DoneItem key={todoTask.id} task={todoTask}></DoneItem>
            ))}
        </div>
    );

}