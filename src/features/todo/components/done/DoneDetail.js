import { useParams } from "react-router-dom"
import { useSelector } from 'react-redux'
export default function DoneDetial() {
    const { id } = useParams();
    const todoTask = useSelector(state => state.todo.tasks).find(task => task.id === id);
    return (
        <div>
            <h1>DoneDetial</h1>
            <div>{todoTask?.id}</div>
            <div>{todoTask?.name}</div>
        </div>
    )
}