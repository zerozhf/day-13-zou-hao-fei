import TodoGenerator from "./TodoGenerator";
import TodoGroup from "./TodoGroup";
import { useEffect } from "react";
import { useTodo } from "../../hooks/useTodo";
export default function TodoList() {
    const { loadTodos } = useTodo()
    useEffect(() => { loadTodos() }, [])
    return (
        <div className='todo-list'>
            <h1>Todo List</h1>
            <TodoGroup />
            <TodoGenerator />
        </div>
    );
} 