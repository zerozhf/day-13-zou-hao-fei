

import { NavLink, Outlet } from 'react-router-dom';
import './App.css';
import { Menu } from 'antd';
import { HomeOutlined, CheckOutlined, QuestionCircleOutlined } from '@ant-design/icons';

function App() {
  return (
    <div className="app">
      <Menu mode="horizontal" className="nav-bar" breakpoint="xl"> {/* 设置breakpoint为'xl' */}
        <Menu.Item key="home" icon={<HomeOutlined />}>
          <NavLink to='/'>Home</NavLink>
        </Menu.Item>
        <Menu.Item key="done" icon={<CheckOutlined />}>
          <NavLink to='/done'>DoneList</NavLink>
        </Menu.Item>
        <Menu.Item key="help" icon={<QuestionCircleOutlined />}>
          <NavLink to='/help'>Help</NavLink>
        </Menu.Item>
      </Menu>
      <Outlet />
    </div>
  );
}

export default App;


