import api from "./api";

export const getToDoTask = () => {
    return api.get("/todos")
}

export const updateToDoTask = (id, todoTask) => {
    return api.put(`/todos/${id}`, todoTask)
}

export const deleteToDoTask = (id) => {
    return api.delete(`/todos/${id}`)
}
export const createToDoTask = (todoTask) => {
    return api.post(`/todos`, todoTask)
}

export const getToDoTaskById = (id) => {
    return api.get(`/todos/${id}`)
}

