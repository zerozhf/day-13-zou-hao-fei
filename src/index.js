import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import store from './store'
import { Provider } from 'react-redux'
import ErrorPage from './pages/ErrorPage';
import HelpPage from './pages/HelpPage';
import DoneList from './features/todo/components/done/DoneList';
import TodoList from './features/todo/components/TodoList';

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import DoneDetial from './features/todo/components/done/DoneDetail';

const root = ReactDOM.createRoot(document.getElementById('root'));

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        index: true,
        element: <TodoList />,
        errorElement: <ErrorPage />,
      },
      {
        path: "/help",
        element: <HelpPage />,
        errorElement: <ErrorPage />
      },
      {
        path: "/done",
        element: <DoneList />,
        errorElement: <ErrorPage />
      },
      {
        path: "/done/:id",
        element: <DoneDetial />,
        errorElement: <ErrorPage />
      },
    ]
  },
]);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
    </Provider>
  </React.StrictMode>
);
